import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("onBackgroundMessage: ${message.data['name']}");
  flutterLocalNotificationsPlugin.show(
      message.notification.hashCode,
      message.notification!.title,
      message.notification!.body,
      NotificationDetails(
        android: AndroidNotificationDetails(chanel.id, chanel.name,
            icon: '@mipmap/ic_launcher',
            playSound: true,
            channelShowBadge: true,
            sound: RawResourceAndroidNotificationSound('pop')),
      ));
}

const AndroidNotificationChannel chanel = AndroidNotificationChannel(
    'high_importance_chanel', 'High Importance Notification',
    importance: Importance.high,
    sound: RawResourceAndroidNotificationSound('pop'));

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(chanel);

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String kor = "ayam bakar";
  @override
  void initState() {
    super.initState();

    var _initialize__ = AndroidInitializationSettings('@mipmap/ic_launcher');
    var _initializeSettings = InitializationSettings(android: _initialize__);
    flutterLocalNotificationsPlugin.initialize(_initializeSettings);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(chanel.id, chanel.name,
                  playSound: true,
                  importance: Importance.max,
                  priority: Priority.high,
                  enableVibration: true,
                  sound: RawResourceAndroidNotificationSound('pop')),
            ));
      }
    });

    getToken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [Text("hai"), Text(kor)],
        ),
      ),
    ));
  }

  getToken() async {
    String? token = await FirebaseMessaging.instance.getToken();
    print(token!);
  }

  controlTester() {}
}
